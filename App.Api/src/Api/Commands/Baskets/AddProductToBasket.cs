using Shared.Messages;
using System;


namespace Api.Commands.Baskets
{
    [MessageNamespace("customers")]
    public class AddProductToBasket : ICommand
    {
        public Guid ProductId { get; }
        public int Quantity { get; }

        public AddProductToBasket(Guid productId, int quantity)
        {
            ProductId = productId;
            Quantity = quantity;
        }
    }
}